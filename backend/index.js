import express from 'express'
import cors from 'cors'
import entryRoutes from './src/entries'
import loginRoute from './src/login'
import resumeRoutes from './src/resumeUpdates'

const app = express()
const PORT = process.env.PORT || 4000

app.use(express.json());
app.use(cors());

// contact form entries, admin login, update resume 
app.use('/contact_form/entries', entryRoutes);
app.use ('/admin_login', loginRoute);
app.use ('/update_resume', resumeRoutes);


app.listen(PORT, () => {
    console.log(`Server started at http://localhost:${PORT}`)
})
