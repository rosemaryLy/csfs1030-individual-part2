import express from 'express';
import * as jwtGenerator from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import adminLogin from '../database/loginPageDatabase';

const router = express.Router();

router.post("/", async (req, res) => {
  try {
    const { name, password } = req.body;
    const user = await adminLogin(name);

    if (user.length > 0) {
      const validPass = await bcrypt.compare(password, user[0].password);

      if (validPass) {
        const token = jwtGenerator.sign({ name }, process.env.JWT_SECRET, {expiresIn: "20m",}
        );

        return res.json({ token });
      } else {
        return error;
      }
    }
  } catch (error) {
    return res.status(401).send({ error: "username or password is incorrect" });
  }
});

export default router;
