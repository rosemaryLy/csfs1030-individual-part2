import express from 'express'
import jwt from 'express-jwt'
import * as contactdb from '../database/contactPageDatabase'
import {validEntry} from './validateEntries'

const router = express.Router()

router.post('/', validEntry, async (req, res) => {
    const newEntry = { ...req.body}
    console.log(newEntry, req.body)
    await contactdb.add(newEntry)
    return res.send(newEntry)
})

router.use(jwt({secret: process.env.JWT_SECRET}))

router.get('/', async (req, res) => {
    res.send(await contactdb.getAll())
})

export default router
