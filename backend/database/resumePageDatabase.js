import { poolConnection } from "./databaseConnection";

// gets all work experience
const getWorkExperience = async () => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query("SELECT * FROM personalsite.workexperience", (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
      });
      return rows;
    });
    return result;
  };

// add new work experience
const newWorkExperience = async (data) => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query("INSERT INTO personalsite.workexperience SET ?", data, (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
      });
      return rows;
    });
    return result;
  };

// update work experience
const updateWorkExperience = async (data) => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query(
          "UPDATE personalsite.workexperience SET jobTitle = ?, employer = ?, startDate = ?, endDate= ?, jobDetails = ?",
          data,
          (err, rows) => {
            if (err) {
              reject(err);
            } else {
              resolve(rows);
            }
          }
        );
      });
      return rows;
    });
    return result;
  };

// delete work experience
const deleteWorkExperience = async (data) => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query(
          "DELETE FROM personalsite.workexperience WHERE jobTitle =?",
          data,
          (err, rows) => {
            if (err) {
              reject(err);
            } else {
              resolve(rows);
            }
          }
        );
      });
      return rows;
    });
    return result;
  };

//get all education
const getEducation = async () => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query("SELECT * FROM personalsite.education", (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
      });
      return rows;
    });
    return result;
  };

// add new education
const newEducation= async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query("INSERT INTO personalsite.education SET ?", data, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
    return rows;
  });
  return result;
};

// update education
const updateEducation = async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query(
        "UPDATE personalsite.education SET schoolName = ?, gradeDate = ?, programName = ?",
        data,
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
    return rows;
  });
  return result;
};

// delete education
const deleteEducation = async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM personalsite.edcuation WHERE schoolName =?",
        data,
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
    return rows;
  });
  return result;
};

//get all qualification
const getQualification = async () => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query("SELECT * FROM personalsite.qualifications", (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
      });
      return rows;
    });
    return result;
  };

// add new qualification
const newQualification= async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query("INSERT INTO personalsite.qualifications SET ?", data, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
    return rows;
  });
  return result;
};

// update qualification
const updateQualification = async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query(
        "UPDATE personalsite.qualificaitons SET qualificationDesc = ?",
        data,
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
    return rows;
  });
  return result;
};

// delete qualification
const deleteQualification = async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM personalsite.qualifications WHERE qualificationID =?",
        data,
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
    return rows;
  });
  return result;
};

//get all additional experience
const getAdditionalExp = async () => {
    const result = await poolConnection(async (connection) => {
      const rows = await new Promise((resolve, reject) => {
        connection.query("SELECT * FROM personalsite.additionalexperience", (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        });
      });
      return rows;
    });
    return result;
  };

// add new additional experience
const newAdditionalExp= async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query("INSERT INTO personalsite.additionalexperience SET ?", data, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
    return rows;
  });
  return result;
};

// update additional experience
const updateAdditionalExp = async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query(
        "UPDATE personalsite.additionalexperience SET experienceDesc = ?",
        data,
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
    return rows;
  });
  return result;
};

// delete additional experience
const deleteAdditionalExperience = async (data) => {
  const result = await poolConnection(async (connection) => {
    const rows = await new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM personalsite.additionalexperience WHERE experienceID =?",
        data,
        (err, rows) => {
          if (err) {
            reject(err);
          } else {
            resolve(rows);
          }
        }
      );
    });
    return rows;
  });
  return result;
};

export { 
    getWorkExperience,
    newWorkExperience,
    updateWorkExperience,
    deleteWorkExperience,
    getEducation,
    newEducation,
    updateEducation,
    deleteEducation,
    getQualification,
    newQualification,
    updateQualification,
    deleteQualification,
    getAdditionalExp,
    newAdditionalExp,
    updateAdditionalExp,
    deleteAdditionalExperience,
};